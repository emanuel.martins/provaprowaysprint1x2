package br.com.proway.provaProwaySprint1x2.models;

public class Casa {
    public int id;
    public String rua;
    public int numero;

    public Casa(int id, String rua, int numero) {
        this.id = id;
        this.rua = rua;
        this.numero = numero;
    }
}
