package br.com.proway.provaProwaySprint1x2.models;

import java.util.ArrayList;

public class DadosCasa {
    public ArrayList<Casa> listaCasas = new ArrayList<>();

    public void inserirCasa(Casa casa) {
        listaCasas.add(casa);
    }

    public void excluirCasa(int id) {
        listaCasas.removeIf(casa -> casa.id == id);
    }
}
