package br.com.proway.provaProwaySprint1x2;

import br.com.proway.provaProwaySprint1x2.models.Casa;
import br.com.proway.provaProwaySprint1x2.models.DadosCasa;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        DadosCasa dados = new DadosCasa();
        String texto;
        int id = 1;

        while(true) {
            texto = "       Sistema de Casas\n";
            for (int i = 0; i < dados.listaCasas.size(); i++) {
                texto += "ID: " + dados.listaCasas.get(i).id + "\n Rua/Av: " + dados.listaCasas.get(i).rua + ", Número: " +
                    dados.listaCasas.get(i).numero + "\n\n";
            }

            String[] opcoes = {"Adicionar", "Excluir", "Sair"};
            int numeroOpcaoEscolhida = JOptionPane.showOptionDialog(null, texto,
                    "Prova Proway - Emanuel Martins( Sistema de Casas)", JOptionPane.DEFAULT_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

            if (numeroOpcaoEscolhida == 0) {
                String rua = JOptionPane.showInputDialog("Rua: ");
                int numero = Integer.parseInt(JOptionPane.showInputDialog("Número (Casa ou Apto): "));

                dados.inserirCasa(new Casa(id, rua, numero));
                id++;
            }

            if (numeroOpcaoEscolhida == 1) {
                int idResidencia = Integer.parseInt(
                        JOptionPane.showInputDialog("ID da residência que deseja remover: "));
                dados.excluirCasa(idResidencia);
            }

            if (numeroOpcaoEscolhida == 2) {
                break;
            }
        }
    }
}
